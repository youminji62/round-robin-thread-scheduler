# Round-Robin Thread Scheduler 구현

![image-20210614063203093](./doc/컨셉.png)

[system flow](https://gitlab.com/youminji62/round-robin-thread-scheduler/-/blob/master/doc/%EC%BB%A8%EC%85%89.PNG)



## 프로젝트 설명

스케줄링 알고리즘 중 하나인 Round-Robin 방식으로 동작하는 스레드 스케줄러 개발 



## 주요 내용

- 레디큐(ready queue), 웨잇큐(wait queue) 구현

- 타이머 시스템 콜을 통해 타임 슬라이스마다 큐잉
- pthread api와 유사하게, thread_join, thread_cancel, thread_suspend 등의 스레드 제어 함수 구현 



## 상세 설명

#### Part 1. 

Queue 구현 레디큐, 웨잇큐에 사용되는 큐는 모두 doubly linked list로 구성되어 있습니다.

linked list를 구현할 때 사용하는 동적 메모리 할당은 malloc 함수를 사용했습니다.

과제의 요구사항이었으므로 어쩔 수 없이 malloc 함수를 사용했지만, 스케줄러는 실시간성이 중요하기 때문에 리눅스 커널 함수를 사용하면 개선된 성능을 낼 수 있을 거라 생각합니다.

#### Part 2.

- thread_api 구현 pthread_API와 유사하게 thread_API를 만들어 스레드를 제어했습니다.

각 함수의 기능은 아래와 같습니다.  thread_create : TCB(Thread Control Block)를 할당하여 레디큐에 넣습니다. 

- thread_suspend : 타겟 스레드를 block하고, TCB를 레디큐에서 꺼내어 웨잇큐에 넣습니다.

- thread_join : 타깃 스레드가 종료할 때까지 caller 스레드가 block됩니다. 

- thread_cancel : 타깃 스레드를 레디큐나 웨잇큐에서 빼내고, TCB 할당을 해제합니다. 

- thread_resume : 타깃 스레드를 웨잇큐에서 찾아서 레디큐에 넣습니다. 